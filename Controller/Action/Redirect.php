<?php

namespace Abitmedia\Pagomedios\Controller\Action;

use Abitmedia\Pagomedios\Logger\Logger;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;

class Redirect extends Action
{
    protected $checkoutSession;
    protected $orderFactory;
    protected $resultRedirectFactory;
    /**
     * @var Http
     */
    private $request;
    /**
     * @var Logger
     */
    private $logger;

    public function __construct(
        Context $context,
        Session $checkoutSession,
        OrderFactory $orderFactory,
        RedirectFactory $resultRedirectFactory,
        Http $request,
        Logger $logger
    )
    {
        parent::__construct($context);

        $this->checkoutSession = $checkoutSession;
        $this->orderFactory = $orderFactory;

        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->request = $request;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->debug('::Redirect init');
        $url = $this->checkoutSession->getRedirectUrl();
        $this->logger->debug('::Redirect URL: ' . $url);

        if ($url === '') {
            $this->messageManager->addErrorMessage(__('No redirect url set.'));
            $this->_redirect('checkout');
        }


        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setUrl($url);

        return $resultRedirect;
    }

    /**
     * @return Order
     */
    protected function getCurrentOrder(): Order
    {
        $order_id = $this->checkoutSession->getLastRealOrderId();

        return $this->orderFactory->create()->loadByIncrementId($order_id);
    }
}