<?php

namespace Abitmedia\Pagomedios\Controller\Action;

use Abitmedia\Pagomedios\Api\Data\ReferenceInterface;
use Abitmedia\Pagomedios\Logger\Logger;
use Abitmedia\Pagomedios\Model\Reference;
use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Service\InvoiceService;
use Abitmedia\Pagomedios\Model\ResourceModel\Reference as ReferenceResource;

class Success extends Action implements
    HttpPostActionInterface,
    HttpGetActionInterface
{
    public $request;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var Order\Email\Sender\OrderSender
     */
    private $orderSender;
    /**
     * @var Order\Email\Sender\InvoiceSender
     */
    private $invoiceSender;
    /**
     * @var InvoiceService
     */
    private $invoiceService;
    /**
     * @var Transaction
     */
    private $transaction;
    /**
     * @var \Abitmedia\Pagomedios\Model\ReferenceFactory
     */
    private $referenceFactory;
    /**
     * @var ReferenceResource
     */
    private $referenceResource;

    public function __construct(
        Context $context,
        Http $request,
        OrderRepository $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Logger $logger,
        Order\Email\Sender\OrderSender $orderSender,
        Order\Email\Sender\InvoiceSender $invoiceSender,
        InvoiceService $invoiceService,
        Transaction $transaction,
        \Abitmedia\Pagomedios\Model\ReferenceFactory $referenceFactory,
        ReferenceResource $referenceResource
    )
    {
        parent::__construct($context);

        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;

        $this->logger = $logger;
        $this->orderSender = $orderSender;
        $this->invoiceSender = $invoiceSender;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->referenceFactory = $referenceFactory;
        $this->referenceResource = $referenceResource;
    }

    public function execute()
    {
        $this->logger->debug('::Success Callback');
        $this->logger->debug('data', $this->request->getPostValue());

        if (!$this->request->has('status')) {
            return;
        }

        if (!$this->request->getPostValue('status') === 'Pagado') {
            $this->messageManager->addErrorMessage(__('Your payment is not authorized, please try again.'));
            $this->_redirect('checkout/onepage/failure');
        }

        try {
            $order = $this->getOrderOrThrows($this->request->getPostValue('reference'));
            $this->processOrder($order);
            $this->saveReference($order);

            $this->_redirect('checkout/onepage/success');
        } catch (Exception $e) {
            $this->_redirect('checkout/onepage/failure');
        }
    }

    /**
     * @param string $orderId
     * @return OrderInterface
     * @throws Exception
     */
    protected function getOrderOrThrows(string $orderId): OrderInterface
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('increment_id', $orderId, 'eq')
            ->create();
        $orderList = $this->orderRepository->getList($searchCriteria)
            ->getItems();

        foreach ($orderList as $order) {
            return $order;
        }

        throw new Exception(sprintf('Order %s does not exists.', $orderId));
    }

    /**
     * @param OrderInterface $order
     * @throws LocalizedException
     */
    private function processOrder(OrderInterface $order)
    {
        /** @var Order $order */
        $order->setState(Order::STATE_PROCESSING);
        $order->setStatus(Order::STATE_PROCESSING);

        $this->orderSender->send($order);
        $order->addStatusToHistory(
            $order->getStatus(),
            __('Pagomedios authorized the payment (#%1).')
        );

        $order->save();
        $invoice = $this->invoiceService->prepareInvoice($order);

        if ($invoice->getTotalQty()) {
            $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
            $invoice->register();

            $this->invoiceSender->send($invoice);
            $this->transaction->addObject($invoice);
            $this->transaction->addObject($invoice->getOrder());
            $this->transaction->save();
        }
    }

    /**
     * @param OrderInterface $order
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function saveReference(OrderInterface $order): void
    {
        /** @var Reference $reference */
        $reference = $this->referenceFactory->create();

        $data = $this->request->getPostValue();

        $reference->setData('authorization_code', $data['authorizationCode']);
        $reference->setData('status', $data['status']);
        $reference->setData('reference', $data['reference']);
        $reference->setData('client_id', $data['clientId']);
        $reference->setData('transaction_date', $data['transactionDate']);
        $reference->setData('message', $data['message']);
        $reference->setData('card_number', $data['cardNumber']);
        $reference->setData('card_brand', $data['cardBrand']);
        $reference->setData('card_holder', $data['cardHolder']);
        $reference->setData('ip_address', $data['ipAddress']);

        $reference->setData(
            'last_trans_id',
            $order->getPayment()->getLastTransId()
        );

        $this->referenceResource->save($reference);
    }
}
