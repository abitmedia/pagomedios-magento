<?php
declare(strict_types=1);

namespace Abitmedia\Pagomedios\Model;

use Abitmedia\Pagomedios\Api\Data\ReferenceInterface;
use Abitmedia\Pagomedios\Api\Data\ReferenceSearchResultInterface;
use Abitmedia\Pagomedios\Api\ReferenceRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\NoSuchEntityException;
use Abitmedia\Pagomedios\Model\ResourceModel\Reference\CollectionFactory as ReferenceCollectionFactory;
use Abitmedia\Pagomedios\Api\Data\ReferenceSearchResultInterfaceFactory;
use Abitmedia\Pagomedios\Model\ResourceModel\Reference\Collection;

class ReferenceRepository implements ReferenceRepositoryInterface
{
    /**
     * @var ReferenceFactory
     */
    private $referenceFactory;
    /**
     * @var ReferenceCollectionFactory
     */
    private $referenceCollectionFactory;
    /**
     * @var ReferenceSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    public function __construct(
        \Abitmedia\Pagomedios\Model\ReferenceFactory $referenceFactory,
        ReferenceCollectionFactory $referenceCollectionFactory,
        ReferenceSearchResultInterfaceFactory $referenceSearchResultInterfaceFactory
    )
    {
        $this->referenceFactory = $referenceFactory;
        $this->referenceCollectionFactory = $referenceCollectionFactory;
        $this->searchResultFactory = $referenceSearchResultInterfaceFactory;
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }

    public function getByReference($reference)
    {
        // TODO: Implement getByReference() method.
    }

    public function getByLastTransId($lastTransId)
    {
        /*$product = $this->productFactory->create();

        $product->load($productId);
        if (!$product->getId()) {
            throw new NoSuchEntityException(
                __("The product that was requested doesn't exist. Verify the product and try again.")
            );
        }

        return $productId;*/
    }

    public function save(ReferenceInterface $hamburger)
    {
        // TODO: Implement save() method.
    }

    public function delete(ReferenceInterface $hamburger)
    {
        // TODO: Implement delete() method.
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->referenceCollectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $collection);
        $this->addSortOrdersToCollection($searchCriteria, $collection);
        $this->addPagingToCollection($searchCriteria, $collection);

        $collection->load();

        return $this->buildSearchResult($searchCriteria, $collection);
    }

    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            $fields = $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $fields[] = $filter->getField();
                $conditions[] = [$filter->getConditionType() => $filter->getValue()];
            }
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        foreach ((array) $searchCriteria->getSortOrders() as $sortOrder) {
            $direction = $sortOrder->getDirection() == SortOrder::SORT_ASC ? 'asc' : 'desc';
            $collection->addOrder($sortOrder->getField(), $direction);
        }
    }

    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $collection->setPageSize($searchCriteria->getPageSize());
        $collection->setCurPage($searchCriteria->getCurrentPage());
    }

    private function buildSearchResult(SearchCriteriaInterface $searchCriteria, Collection $collection)
    {
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }
}