<?php
namespace Abitmedia\Pagomedios\Model;

abstract class AbstractMethod extends \Magento\Payment\Model\Method\AbstractMethod
{
    protected $_supportedCurrencyCodes = [
        'USD', 'PEN', 'COP',
    ];

    /**
     * @return bool
     */
    public function isMethodActive()
    {
        return $this->getConfigData('active');
    }

    /**
     * @return array
     */
    public function getSupportedCurrencies()
    {
        return explode(',', $this->getConfigData('currency'));
    }
}
