<?php
declare(strict_types=1);

namespace Abitmedia\Pagomedios\Model\ResourceModel;

class Reference extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    const MAIN_TABLE = 'pagomedios_references';
    const PRIMARY_KEY = 'entity_id';

    protected function _construct()
    {
        $this->_init(self::MAIN_TABLE, self::PRIMARY_KEY);
    }
}