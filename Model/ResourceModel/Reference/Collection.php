<?php
declare(strict_types=1);

namespace Abitmedia\Pagomedios\Model\ResourceModel\Reference;

use Abitmedia\Pagomedios\Model\Reference as Model;
use Abitmedia\Pagomedios\Model\ResourceModel\Reference as ResourceModel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}