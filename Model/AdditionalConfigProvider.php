<?php

namespace Abitmedia\Pagomedios\Model;

use Abitmedia\Pagomedios\Logger\Logger;
use Abitmedia\Pagomedios\Model\Source\CompanyType as CompanyTypeSource;
use Abitmedia\Pagomedios\Model\Source\DocumentType as DocumentTypeSource;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Payment\Gateway\Config\Config;

class AdditionalConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @var Repository
     */
    private $assetRepository;
    /**
     * @var Logger
     */
    private $logger;
    /**
     * @var CompanyTypeSource
     */
    private $companyType;
    /**
     * @var DocumentTypeSource
     */
    private $documentType;

    /**
     * Initialize dependencies.
     *
     * @param Config $config
     * @param EncryptorInterface $encryptor
     * @param Repository $assetRepository
     * @param Logger $logger
     * @param CompanyTypeSource $companyType
     * @param DocumentTypeSource $documentType
     */
    public function __construct(
        Config $config,
        EncryptorInterface $encryptor,
        Repository $assetRepository,
        Logger $logger,
        CompanyTypeSource $companyType,
        DocumentTypeSource $documentType
    )
    {
        $this->config = $config;
        $this->encryptor = $encryptor;
        $this->assetRepository = $assetRepository;
        $this->logger = $logger;
        $this->companyType = $companyType;
        $this->documentType = $documentType;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig(): array
    {
        if ($this->config->getValue('active') == 0) {
            return $this->submitConfig(false);
        }

        return $this->submitConfig(true, [
            'image' => $this->assetRepository->getUrl('Abitmedia_Pagomedios::images/pagomedios_brand.png'),
            'image_credit_cards' => $this->assetRepository->getUrl('Abitmedia_Pagomedios::images/pagomedios_credit_cards.png'),
            'textQuestion' => '',
            'urlQuestion' => '',
            'paymentDescription' => '',
        ]);
    }

    /**
     * @param bool $active
     * @param array $data
     * @return array|array[]
     */
    private function submitConfig(bool $active, array $data = []): array
    {
        $response = array_merge($data, [
            'isActive' => (string)((int)$active),
            'title' => 'Pagomedios',
            'description' => $this->config->getValue('store_desc'),
            'billing_types' => $this->companyType->toOptionArray(),
            'document_types' => $this->documentType->toOptionArray(),
        ]);

        return [
            'payment' => [
                Method::CODE => $response
            ]
        ];
    }
}
