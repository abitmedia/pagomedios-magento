<?php
declare(strict_types=1);

namespace Abitmedia\Pagomedios\Model;

use Abitmedia\Pagomedios\Model\ResourceModel\Reference as ResourceModel;

class Reference extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}