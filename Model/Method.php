<?php

namespace Abitmedia\Pagomedios\Model;

use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Sales\Model\Order;

class Method extends AbstractMethod
{
    public const CODE = 'pagomedios';
    public const GATEWAY_URL = 'https://cloud.abitmedia.com/api/payments/create-payment-request';


    protected $_code = self::CODE;

    protected $_isGateway = true;
    protected $_canCapture = false;
    protected $_canOrder = true;

    protected $_canAuthorize = false;
    protected $_canCapturePartial = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;

    protected $_canCancelInvoice = true;
    protected $_canUseInternal = false;
    protected $_isInitializeNeeded = true;

    protected $_isOffline = true;

    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;
    /**
     * @var \Abitmedia\Pagomedios\Logger\Logger
     */
    private $paymentLogger;

    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,

        UrlInterface $url,
        \Abitmedia\Pagomedios\Logger\Logger $paymentLogger,
        \Magento\Checkout\Model\Session $checkoutSession,

        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DirectoryHelper $directory = null
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data,
            $directory
        );

        $this->url = $url;
        $this->_logger = $paymentLogger;
        $this->checkoutSession = $checkoutSession;

        $this->paymentLogger = $paymentLogger;
    }

    /**
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        if (!$this->isMethodActive()) {
            return false;
        }

        return parent::isAvailable($quote);
    }

    /**
     * @param string $currencyCode
     * @return bool
     */
    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, $this->getSupportedCurrencies());
//        return in_array($currencyCode, $this->_supportedCurrencyCodes);
    }
}
