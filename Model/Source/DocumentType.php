<?php

namespace Abitmedia\Pagomedios\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class DocumentType implements OptionSourceInterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '01',
                'label' => 'Cédula de identidad'
            ],
            [
                'value' => '02',
                'label' => 'RUC'
            ],
            [
                'value' => '03',
                'label' => 'Pasaporte'
            ],
            [
                'value' => '04',
                'label' => 'ID del exterior'
            ],
        ];
    }
}