<?php

namespace Abitmedia\Pagomedios\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class PaymentGateway implements OptionSourceInterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '1',
                'label' => 'Alignet Payme'
            ],
            [
                'value' => '2',
                'label' => 'Payphone'
            ],
            [
                'value' => '3',
                'label' => 'Datafast'
            ],
            [
                'value' => '4',
                'label' => 'Paymentez'
            ],
        ];
    }
}