<?php

namespace Abitmedia\Pagomedios\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class CompanyType implements OptionSourceInterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'Persona Natural',
                'label' => 'Persona Natural'
            ],
            [
                'value' => 'Empresa',
                'label' => 'Empresa'
            ],
        ];
    }
}