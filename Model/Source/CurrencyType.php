<?php
namespace Abitmedia\Pagomedios\Model\Source;

use Magento\Config\Model\Config\Source\Locale\Currency;

class CurrencyType extends Currency
{
    /**
     * Allowed Currency types
     *
     * @var array
     */
    protected $_allowedTypes = [
        'USD',
        'COP',
        'PEN',
    ];

    public function toOptionArray()
    {
        $options = [];
        $currencies = parent::toOptionArray();

        foreach ($currencies as $currency)
        {
            if (in_array($currency['value'], $this->_allowedTypes))
            {
                $options[] = $currency;
            }
        }

        return $options;
    }
}