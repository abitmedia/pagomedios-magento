<?php

namespace Abitmedia\Pagomedios\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ElectronicBilling implements OptionSourceInterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '0',
                'label' => __('No')
            ],
            [
                'value' => '1',
                'label' => __('Yes')
            ],
        ];
    }
}