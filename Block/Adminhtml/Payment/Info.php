<?php

namespace Abitmedia\Pagomedios\Block\Adminhtml\Payment;

use Abitmedia\Pagomedios\Model\Method;
use Abitmedia\Pagomedios\Model\ReferenceRepository;
use Magento\Framework\Phrase;
use Magento\Payment\Block\ConfigurableInfo;
use Magento\Sales\Model\Order;
use Abitmedia\Pagomedios\Model\ReferenceFactory;
use Abitmedia\Pagomedios\Model\ResourceModel\Reference\CollectionFactory as ReferenceCollectionFactory;

class Info extends ConfigurableInfo
{
    public $charge = null;
    public $cards = array();
    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;
    /**
     * @var \Magento\Payment\Model\Info
     */
    private $info;
    /**
     * @var \Magento\Directory\Model\Country
     */
    private $country;
    /**
     * @var ReferenceRepository
     */
    private $referenceRepository;
    /**
     * @var ReferenceFactory
     */
    private $referenceFactory;
    /**
     * @var ReferenceCollectionFactory
     */
    private $referenceCollectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Payment\Gateway\ConfigInterface $config,
        \Magento\Directory\Model\Country $country,
        \Magento\Payment\Model\Info $info,
        \Magento\Framework\Registry $registry,
        ReferenceRepository $referenceRepository,
        ReferenceFactory $referenceFactory,
        ReferenceCollectionFactory $referenceCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $config, $data);
        $this->country = $country;
        $this->info = $info;
        $this->registry = $registry;

        $this->referenceRepository = $referenceRepository;
        $this->referenceFactory = $referenceFactory;
        $this->referenceCollectionFactory = $referenceCollectionFactory;
    }

    public function shouldDisplayStripeSection()
    {
        $charge = $this->getCharge();
        $isCard = false;

        if ($charge)
            $isCard = true;

        return ($this->isPagomediosMethod() && $isCard);
    }

    public function getMethod()
    {
        $order = $this->registry->registry('current_order');
        return $order->getPayment();
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return $this->registry->registry('current_order');
    }

    public function getInfo()
    {
        $payment = $this->getMethod();
        $this->info->setData($payment->getData());
        return $this->info;
    }

    public function getCard()
    {
        $charge = $this->getCharge();

        if (empty($charge))
            return null;

        if (!empty($charge->source))
        {
            if (isset($charge->source->object) && $charge->source->object == 'card')
                return $charge->source;

            if (isset($charge->source->type) && $charge->source->type == 'three_d_secure')
            {
                $cardId = $charge->source->three_d_secure->card;
                if (isset($this->cards[$cardId]))
                    return $this->cards[$cardId];

                $card = new \stdClass();
                $card = $charge->source->three_d_secure;
                $this->cards[$cardId] = $card;

                return $this->cards[$cardId];
            }
        }

        // Payment Methods API
        if (!empty($charge->payment_method_details->card))
            return $charge->payment_method_details->card;

        // Sources API
        if (!empty($charge->source->card))
            return $charge->source->card;

        return null;
    }

    public function isPagomediosMethod()
    {
        $method = $this->getMethod()->getMethod();

        if (strpos($method, Method::CODE) !== 0)
            return false;

        return true;
    }

    public function getCharge()
    {
        if (!$this->isPagomediosMethod())
            return null;

        if (!empty($this->charge))
            return $this->charge;

        if ($this->charge === false)
            return false;

        try
        {
            $collection = $this->referenceCollectionFactory->create();

            $reference = $collection->addFieldToFilter('reference', $this->getOrder()->getIncrementId());
            $this->charge = $reference;
        }
        catch (\Exception $e)
        {
            $this->charge = false;
        }

        return $this->charge;
    }

    public function getCustomerId()
    {
        $charge = $this->getCharge();

        if (isset($charge->customer) && !empty($charge->customer))
            return $charge->customer;

        return null;
    }

    public function getPaymentId()
    {
        $charge = $this->getCharge();

        if (isset($charge->id))
            return $charge->id;

        return null;
    }

    public function getMode()
    {
        $charge = $this->getCharge();

        if ($charge->livemode)
            return "";

        return "test/";
    }

    public function getSourceType()
    {
        $charge = $this->getCharge();

        if (!isset($charge->source->type))
            return null;

        return ucwords(str_replace("_", " ", $charge->source->type));
    }
}
