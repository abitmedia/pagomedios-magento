<?php
namespace Abitmedia\Pagomedios\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Abitmedia\Pagomedios\Api\Data\ReferenceSearchResultInterface;
use Abitmedia\Pagomedios\Api\Data\ReferenceInterface;

interface ReferenceRepositoryInterface
{
    /**
     * @param int $id
     * @return ReferenceInterface
     * @throws NoSuchEntityException
     */
    public function getById($id);

    /**
     * @param string $reference
     * @return ReferenceInterface
     * @throws NoSuchEntityException
     */
    public function getByReference($reference);

    /**
     * @param string $lastTransId
     * @return ReferenceInterface
     * @throws NoSuchEntityException
     */
    public function getByLastTransId($lastTransId);

    /**
     * @param ReferenceInterface $hamburger
     * @return ReferenceInterface
     */
    public function save(ReferenceInterface $hamburger);

    /**
     * @param ReferenceInterface $hamburger
     * @return void
     */
    public function delete(ReferenceInterface $hamburger);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return ReferenceSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}