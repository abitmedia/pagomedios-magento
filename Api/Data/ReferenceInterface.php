<?php
namespace Abitmedia\Pagomedios\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ReferenceInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return void
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getAuthorizationCode();

    /**
     * @param int $authorizationCode
     * @return void
     */
    public function setAuthorizationCode($authorizationCode);

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @param string $status
     * @return void
     */
    public function setStatus($status);

    /**
     * @return string
     */
    public function getCardNumber();

    /**
     * @param string $cardNumber
     * @return void
     */
    public function setCardNumber($cardNumber);
}