<?php
namespace Abitmedia\Pagomedios\Api\Data;

interface ReferenceSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return ReferenceInterface[]
     */
    public function getItems();

    /**
     * @param ReferenceInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}