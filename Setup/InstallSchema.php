<?php

declare(strict_types=1);

namespace Abitmedia\Pagomedios\Setup;

use Abitmedia\Pagomedios\Model\ResourceModel\Reference;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $this->createMyTable($setup);
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws Zend_Db_Exception
     */
    private function createMyTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable(Reference::MAIN_TABLE);
        $table = $setup->getConnection()->newTable(
            $tableName
        )->addColumn(
            Reference::PRIMARY_KEY,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'entity_id'
        )->addColumn(
            'authorization_code',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'default' => 0]
        )->addColumn(
            'status',
            Table::TYPE_TEXT,
            50,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'reference',
            Table::TYPE_TEXT,
            100,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'client_id',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'transaction_date',
            Table::TYPE_TEXT,
            100,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'message',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'card_number',
            Table::TYPE_TEXT,
            50,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'card_brand',
            Table::TYPE_TEXT,
            50,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'card_holder',
            Table::TYPE_TEXT,
            50,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'ip_address',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'last_trans_id',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => '']
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Create date'
        )->setComment(
            'Abitmedia_Pagomedios Reference Table'
        );
        $setup->getConnection()->createTable($table);
    }
}