<?php

namespace Abitmedia\Pagomedios\Observer;

use Abitmedia\Pagomedios\Logger\Logger;
use Abitmedia\Pagomedios\Model\Method;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class SaveOrderAfterSubmitObserver implements ObserverInterface
{
    /**
     * @var Registry
     */
    protected $coreRegistry;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @var Session
     */
    protected $checkoutSession;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var Logger
     */
    private $paymentLogger;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * SaveOrderAfterSubmitObserver constructor.
     * @param Registry $coreRegistry
     * @param ScopeConfigInterface $scopeConfig
     * @param UrlInterface $url
     * @param Logger $paymentLogger
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Registry $coreRegistry,
        ScopeConfigInterface $scopeConfig,
        UrlInterface $url,
        Logger $paymentLogger,
        Session $checkoutSession,
        StoreManagerInterface $storeManager
    )
    {
        $this->coreRegistry = $coreRegistry;
        $this->_scopeConfig = $scopeConfig;
        $this->url = $url;
        $this->paymentLogger = $paymentLogger;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Observer $observer
     * @throws ValidatorException
     */
    public function execute(Observer $observer)
    {
        /** @var InfoInterface $payment */
        $payment = $observer->getEvent()->getPayment();

        $this->guardRequiredAdditionalInformation($payment);
        $this->processRequestOrder($payment);
    }

    /**
     * @param InfoInterface $payment
     * @throws ValidatorException
     */
    private function guardRequiredAdditionalInformation(InfoInterface $payment)
    {
        if (!$payment->hasAdditionalInformation('billing_company_type')) {
            throw new ValidatorException(__(
                'Billing company type is required.'
            ));
        }

        if (!$payment->hasAdditionalInformation('billing_document_type')) {
            throw new ValidatorException(__(
                'Billing document type is required.'
            ));
        }

        if (!$payment->hasAdditionalInformation('billing_document')) {
            throw new ValidatorException(__(
                'Billing document is required.'
            ));
        }
    }

    /**
     * @param InfoInterface $payment
     * @throws ValidatorException
     */
    private function processRequestOrder(InfoInterface $payment)
    {
        /** @var Order $order */
        /** @var Order\Address $billing */

        $order = $payment->getOrder();
        $billing = $order->getBillingAddress();

        $amount = round($order->getGrandTotal(), 2);
        $withoutIva = round($amount / 1.12, 2);
        $iva = round($amount - $withoutIva, 2);

        $notificationUrl = $this->url->getUrl('pagomedios/action/success');

        $data = [
            'companyType' => $payment->getAdditionalInformation('billing_company_type'),
            'document' => $payment->getAdditionalInformation('billing_document'),
            'documentType' => $payment->getAdditionalInformation('billing_document_type'),
            'fullName' => $billing->getFirstname() . ' ' . $billing->getLastname(),
            'address' => $billing->getStreetLine(1) . ' ' . $billing->getStreetLine(2),
            'mobile' => $billing->getTelephone(),
            'email' => $billing->getEmail(),
            'description' => 'Orden de compra #' . $order->getIncrementId(),

            'amount' => $amount, //Total con IVA
            'amountWithTax' => $withoutIva, //Subtotal con impuestos IVA 12%
            'amountWithoutTax' => 0,  //Subtotal sin impuestos IVA 0%
            'tax' => $iva, //Total impuestos, Total IVA

            'gateway' => $this->getConfigData('payment_gateway'),
            'notifyUrl' => $notificationUrl,
            'reference' => $order->getIncrementId(),
            'order_id' => $order->getRealOrderId(),
            'generateInvoice' => $this->getConfigData('electronic_billing'),
        ];
        $this->paymentLogger->debug('Payment DATA', $data);

        $response = $this->getClientResponse($data);

        $redirectUrl = $response['data']['url'];
        $this->paymentLogger->debug('action URL', [$redirectUrl]);


        $this->checkoutSession->setRedirectUrl($redirectUrl);
    }

    /**
     * @param string $field
     * @return mixed
     */
    private function getConfigData(string $field)
    {
        $path = 'payment/' . Method::CODE . '/' . $field;

        return $this->_scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    /**
     * @param array $data
     * @return array
     * @throws ValidatorException
     */
    private function getClientResponse(array $data)
    {
        $headers = [
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 3724770d-a4c7-4330-97dc-6d66237dbc19",
            "cache-control: no-cache",
            sprintf('Authorization: Bearer %s', $this->getConfigData('commerce_token'))
        ];

        $this->paymentLogger->debug('Request', $data);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, Method::GATEWAY_URL);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $responseData = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->paymentLogger->debug($err);
            throw new ValidatorException(__($err));
        }

        /** @var array $response */
        $response = json_decode($responseData, true);
        $this->paymentLogger->debug('Response', $response);

        if ($response['code'] !== 1) {
            $message = [];
            $this->paymentLogger->debug(json_encode($response['errors']));

            foreach ($response['errors'] as $key => $value) {
                $message[] = is_array($value) ? implode(', ', $value) : $value;
            }

            $this->paymentLogger->debug('errors', $message);
            throw new ValidatorException(__(implode(', ', $message)));
        }

        return $response;
    }
}
