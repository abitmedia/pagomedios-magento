<?php
namespace Abitmedia\Pagomedios\Helper;

class Config extends \Magento\Payment\Gateway\Config\Config
{
    private $responseFormat = 'XML';

    public function getApiKey()
    {
        return $this->getValue('api_key');
    }

    public function getSignatureKey()
    {
        return $this->getValue('signature_key');
    }

    public function getExpirationTime()
    {
        return $this->getValue('expiration_time');
    }

    public function getEnvironmentMode()
    {
        return $this->getValue('environment_mode');
    }

    public function getResponseFormat()
    {
        return $this->responseFormat;
    }
    public function getSuccessfulStatus()
    {
        return $this->getValue('successful_status');
    }
}