<?php

namespace Abitmedia\Pagomedios\Exceptions;

use Exception;
use Abitmedia\Pagomedios\Gateway\Http\OperationResponse;

class GetOperationException extends Exception
{

    public static function responseError(OperationResponse $response)
    {
        return new static(
            __(
                'SafetyPay Operation Error: %1, with code: %2.',
                (string)$response->OperationResponse->ErrorManager->Description,
                (string)$response->OperationResponse->ErrorManager->ErrorNumber
            )
        );
    }
}