<?php
namespace Abitmedia\Pagomedios\Exceptions;

use Exception;
use Abitmedia\Pagomedios\Gateway\Http\ExpressTokenResponse;

class ExpressTokenException extends Exception
{
    public static function responseError(ExpressTokenResponse $response)
    {
        return new static(
            __(
                'SafetyPay Error: %1, with code: %2.',
                (string) $response->ExpressTokenResponse->ErrorManager->Description,
                (string) $response->ExpressTokenResponse->ErrorManager->ErrorNumber
            )
        );
    }
}