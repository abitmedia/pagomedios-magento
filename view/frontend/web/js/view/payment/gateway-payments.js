define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'pagomedios',
                component: 'Abitmedia_Pagomedios/js/view/payment/method-renderer/gateway-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);