define(
    [
        'Magento_Payment/js/view/payment/cc-form',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Payment/js/model/credit-card-validation/validator',
        'mage/url',
        'Magento_Checkout/js/action/place-order'
    ],
    function (Component, $, quote, validator, url, placeOrderAction) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Abitmedia_Pagomedios/payment/payment-form',
                redirectAfterPlaceOrder: false,
                billingCompanyType: '',
                billingDocumentType: '',
            },

            getCode: function() {
                return 'pagomedios';
            },

            isActive: function() {
                return true;
            },

            validate: function() {
                var $form = $('#' + this.getCode() + '-form');
                return $form.validation() && $form.validation('isValid');
            },

            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'billing_company_type': $('[name="payment[billing_company_type]"]').val(),
                        'billing_document_type': $('[name="payment[billing_document_type]"]').val(),
                        'billing_document': $('[name="payment[billing_document]"]').val(),
                    }
                };
            },

            preparePayment: function(data, event)
            {
                if (event) {
                    event.preventDefault();
                }

                let self = this,
                    placeOrder;

                this.isPlaceOrderActionAllowed(false);
                placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);
                $.when(placeOrder).fail(function () {
                    self.isPlaceOrderActionAllowed(true);
                }).done(this.afterPlaceOrder.bind(this));

                return true;
            },

            afterPlaceOrder: function (data, event) {
                let redirectUrl = `pagomedios/action/redirect`;

                window.location.replace(url.build(redirectUrl));
            },

            /**
             * Get list of available billing types.
             * @returns {Object}
             */
            getBillingCompanyType: function() {
                return this.getConfig().billing_types;
            },

            /**
             * Get list of available billing types.
             * @returns {Object}
             */
            getBillingDocumentType: function() {
                return this.getConfig().document_types;
            },

            getEmail: function () {
                if(quote.guestEmail) {
                    return quote.guestEmail;
                } else {
                    return window.checkoutConfig.customerData.email;
                }
            },

            retryPlaceOrder: function()
            {
                this.isPlaceOrderActionAllowed(true);
            },

            getLogo: function (){
                return this.getConfig().image;
            },

            getCreditCardsImages: function (){
                return this.getConfig().image_credit_cards;
            },

            loadCheckoutJS: function()
            {
                console.log('test');
            },

            getHourExpiration: function() {
                return this.getConfig().hourExpiration;
            },
            getTextQuestion: function() {
                return this.getConfig().textQuestion;
            },
            getUrlQuestion: function() {
                return this.getConfig().urlQuestion;
            },
            getPaymentDescription: function() {
                return this.getConfig().paymentDescription;
            },

            getConfig: function() {
                return window.checkoutConfig.payment.pagomedios;
            },
        });
    }
);
